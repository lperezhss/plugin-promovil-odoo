from odoo import fields, models, api


class Partner(models.Model):
    _inherit = 'res.partner'

    names = fields.Char()
    last_names = fields.Char()
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')])
    user_id = fields.Many2one('res.users')