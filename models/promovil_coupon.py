from odoo import _, api, exceptions, fields, models


class ProMovilCoupon(models.Model):
    _name = 'promovil.coupon'
    _description = 'Cupones'

    company_id = fields.Many2one('res.company', string='Empresa', default=lambda self: self.env.company)
    coupon = fields.Char(string="Cupón", copy=False)
    name = fields.Char(string='Nombre', copy=False)
    ip = fields.Char(string='IP')
    protocol = fields.Selection(
        [
            ('http', 'HTTP'),
            ('https', 'HTTPS'),
        ], string="Protocolo", default='http', copy=False
    )
    port = fields.Char(string='Puerto')
    url = fields.Char(string='URL', compute='_compute_url', default=False)
    vendor = fields.Char(string='Vendedor')
    tax = fields.Char(string='Impuesto')
    number = fields.Char(string='Numero')
    view_stock = fields.Boolean(string='Ver Stock', default=True, copy=False)
    view_tax = fields.Boolean(string='Ver Impuesto', default=True, copy=False)
    validate_stock = fields.Boolean(string='Validar Stock', default=True, copy=False)
    validate_balance = fields.Boolean(string='Validar Saldo', default=True, copy=False)
    view_term = fields.Boolean(string='Ver Terminos de Pagos', default=True, copy=False)

    def _compute_url(self):
        for record in self:
            record.url = record.protocol + '://' + record.ip + (':' + record.port if record.port else '') + '/'
