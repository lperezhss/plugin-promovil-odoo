# -*- coding: utf-8 -*-
{
    'version': '1.0.1',

    'name': "Odoo ProMovil",

    'category': 'Services/ProMovil',

    'summary': """
        This module controls the ProMovil
    """,

    'description': """
        This module controls the ProMovil
    """,

    'author': "HSS Solutions",
    'website': "http://www.google.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list

    # any module necessary for this one to work correctly
    'depends': ['base', 'contacts', 'l10n_latam_base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/report_group.xml',
        'data/coupon.xml',
        'views/promovil_coupon.xml',
        'security/main_menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],

    'installable': True,
    'application': True,
    'license': 'LGPL-3',
}