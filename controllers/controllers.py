# -*- coding: utf-8 -*-
import json

import requests

from odoo import http
from odoo.http import request, Response

from odoo.odoo.http import JsonRequest


class ProMovilController(http.Controller):
    @http.route('/api/pm/promovil/VerTipoIdentificacion/<int:client_id>', auth='none', type='http')
    def get_typetaxid(self, client_id=0):
        try:
            body = self.get_pm_response(True, 0, '', '[]')
            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Tipo de Identificacion', '')
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/api/pm/promovil/VerClientes/<int:client_id>/<int:salesrep_id>', auth='none', type='http')
    def get_partners(self, client_id=0, salesrep_id=0):
        try:
            res_partners = http.request.env['res.partner'].sudo().search([])
            datas = []
            counter = 0
            for res_partner in res_partners:
                data = {
                    'c_bpartner_id': res_partner.id,
                    'codigo': res_partner.vat if res_partner.vat else '',
                    'descripcion': res_partner.name if res_partner.name else '',
                    'ruc': res_partner.vat if res_partner.vat else '',
                    'direccion': res_partner.street if res_partner.street else '',
                    'telefono': res_partner.phone if res_partner.phone else '',
                    'zona': '',
                    'email': res_partner.email if res_partner.email else '',
                    'tprecio': '1',
                    'co_ven': 0,
                    'rutero': '',
                    'isdetailednames': 'N',
                    'lco_taxidtype_id': res_partner.l10n_latam_identification_type_id.id,
                    'firstname1': '',
                    'firstname2': '',
                    'lastname1': '',
                    'lastname2': '',
                }

                datas.append(data)

            body = self.get_pm_response(True, 0, '', datas)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Clientes', '')
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/api/pm/promovil/VerDocumentos/<int:client_id>/<int:salesrep_id>', auth='none', type='http')
    def get_documents(self, client_id, salesrep_id):
        try:
            body = self.get_pm_response(True, 0, '', '[]')
            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Documentos', '')
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/api/pm/promovil/VerLineas/<int:client_id>', auth='none', type='http')
    def get_product_category(self, client_id):
        try:
            product_categorys = http.request.env['product.category'].sudo().search([])
            data = []
            counter = 0
            for product_category in product_categorys:
                data += [{
                    'm_product_category_id': product_category.id,
                    'codigo': product_category.name if product_category.name else '',
                    'descripcion': product_category.name if product_category.name else '',
                }]

            body = self.get_pm_response(True, 0, '', data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Lineas', '')
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/api/pm/promovil/VerArticulos/<int:client_id>', auth='none', type='http')
    def get_products(self, client_id):
        try:
            product_templates = http.request.env['product.template'].sudo().search([])
            data = []
            counter = 0
            for product_template in product_templates:
                data += [{
                    'm_product_id': product_template.id,
                    'codigo': product_template.default_code if product_template.default_code else '',
                    'descripcion': product_template.name if product_template.name else '',
                    'referencia': product_template.default_code if product_template.default_code else '',
                    'stock': 0.0,
                    'tipo': product_template.type,
                    'tipoimp': '',
                    'm_product_category_id': 0,
                    'tasa': 0.0,
                    'precio': 0.0,
                    'precio2': 0.0,
                    'precio3': 0.0,
                    'precio4': 0.0,
                    'precio5': 0.0,
                    'univenta1': 'UND',
                    'univenta2': 'UND',
                    'escolortalla': True,
                }]

            body = self.get_pm_response(True, 0, '', data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Articulos', '')
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/api/pm/promovil/VerColores/<int:client_id>', auth='none', type='http')
    def get_colors(self, client_id):
        try:
            body = self.get_pm_response(True, 0, '', '[]')
            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Colores', '')
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/api/pm/promovil/VerTallas/<int:client_id>', auth='none', type='http')
    def get_sizes(self, client_id):
        try:
            body = self.get_pm_response(True, 0, '', '[]')
            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = self.get_pm_response(False, -1, 'Error Consultando Tallas', '')
            return Response(json.dumps(body), headers=self.get_headers())


    def get_headers(self):
        return {
            'Content-Type': 'application/json'
        }


    def get_pm_response(self, status, id, message, json):
        return {
          "id": id,
          "status": status,
          "message": message,
          "json": json
        }


    # Flutter
    @http.route('/coupon', auth='none', type='http')
    def get_coupons(self):
        try:
            coupons = http.request.env['promovil.coupon'].sudo().search([])

            body = []
            for coupon in coupons:
                data = {
                    'coupon': coupon.coupon,
                    'name': coupon.name,
                    'ip': coupon.ip,
                    'protocol': coupon.protocol,
                    'port': coupon.port,
                    'vendor': coupon.vendor,
                    'tax': coupon.tax,
                    'number': coupon.number,
                    'view_stock': 'S' if coupon.view_stock else 'N',
                    'view_tax': 'S' if coupon.view_tax else 'N',
                    'view_term': 'S' if coupon.view_term else 'N',
                    'validate_stock': 'S' if coupon.validate_stock else 'N',
                    'validate_balance': 'S' if coupon.validate_balance else 'N',
                }

                body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Cupones'
            return Response(json.dumps(body), headers=self.get_headers())

    @http.route('/customer', auth='none', type='http')
    def get_customers(self):
        # TODO
        try:
            res_partners = http.request.env['res.partner'].sudo().search([])

            body = []
            for res_partner in res_partners:
                data = {
                    'id': str(res_partner.id),
                    'value': res_partner.vat if res_partner.vat else '',
                    'name': res_partner.name if res_partner.name else '',
                    'taxid': res_partner.vat if res_partner.vat else '',
                    'address': res_partner.street if res_partner.street else '',
                    'phone': res_partner.phone if res_partner.phone else '',
                    'zone': '',
                    'email': res_partner.email if res_partner.email else '',
                    'tprice': '1',
                    'vendor': str(res_partner.user_id.id) if res_partner.user_id else '0',
                    'route': '',
                    'coupon': '0000-0000',
                }

                body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Clientes'
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route(['/document_summary','/document'], auth='none', type='http')
    def get_documents(self):
        # TODO
        try:
            body = []
            data = {
                'customer_id': '0',
                'value': '0',
                'name': 'No Definido',
                'taxid': 'No Definido',
                'doctype': 'No Definido',
                'docnum': 'No Definido',
                'datetrx': '23/09/1980',
                'amount': '100.0',
                'balance': '25.5',
                'coupon': '0000-0000',
            }
            body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Documentos'
            return Response(json.dumps(body), headers=self.get_headers())

    @http.route('/product', auth='none', type='http')
    def get_products(self):
        # TODO
        try:
            product_templates = http.request.env['product.template'].sudo().search([])
            body = []
            counter = 0
            for product_template in product_templates:
                data = {
                    'id': str(product_template.id),
                    'value': product_template.default_code if product_template.default_code else '',
                    'name': product_template.name if product_template.name else '',
                    'category_id': '0',
                    'type': product_template.type,
                    'taxtype': '',
                    'ref': product_template.default_code if product_template.default_code else '',
                    'stock': '10.0',
                    'price1': '20.0',
                    'price2': '20.0',
                    'price3': '20.0',
                    'price4': '20.0',
                    'price5': '20.0',
                    'uom1': 'UND',
                    'uom2': 'UND',
                    'equivalent1': '1',
                    'equivalent2': '1',
                    'coupon': '0000-0000',
                }

                body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Productos'
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/category', auth='none', type='http')
    def get_categories(self):
        # TODO
        try:
            product_categorys = http.request.env['product.category'].sudo().search([])

            body = []
            for product_category in product_categorys:
                data = {
                    'id': str(product_category.id),
                    'value': product_category.name if product_category.name else '',
                    'name': product_category.name if product_category.name else '',
                    'coupon': '0000-0000',
                }

                body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Categorias de Productos'
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/cash_bank', auth='none', type='http')
    def get_cash_banks(self):
        # TODO
        try:
            body = []
            data = {
                'id': '0',
                'value': '0',
                'name': 'No Definido',
                'type': 'No Definido',
                'coupon': '0000-0000',
            }
            body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Caja Bancos'
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/payment_type', auth='none', type='http')
    def get_payment_types(self):
        # TODO
        try:
            body = []
            data = {
                'id': '0',
                'value': '0',
                'name': 'No Definido',
                'coupon': '0000-0000',
            }
            body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Tipo de Pago'
            return Response(json.dumps(body), headers=self.get_headers())


    @http.route('/term', auth='none', type='http')
    def get_terms(self):
        # TODO
        try:
            body = []
            data = {
                'id': '0',
                'value': '0',
                'name': 'No Definido',
                'days': '10',
                'coupon': '0000-0000',
            }
            body.append(data)

            return Response(json.dumps(body), headers=self.get_headers())
        except Exception:
            body = 'Error Consultando Terminos de Pagos'
            return Response(json.dumps(body), headers=self.get_headers())