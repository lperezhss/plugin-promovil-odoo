# ProMovil RA

## VerArticulos
```
http://localhost:8069/api/pm/promovil/VerArticulos/0
```

## VerClientes
```
http://localhost:8069/api/pm/promovil/VerClientes/0/0
```

## VerDocumentos

```
http://localhost:8069/api/pm/promovil/VerDocumentos/0/0
```

# ProMovil Flutter

## Coupon
```
http://localhost:8069/coupon
```

## Customer
```
http://localhost:8069/customer
```

## Document Summary
```
http://localhost:8069/document_summary
```

## Document
```
http://localhost:8069/document
```

## Product
```
http://localhost:8069/product
```

## Category
```
http://localhost:8069/category
```

## Cash Bank
```
http://localhost:8069/cash_bank
```

## Payment Type
```
http://localhost:8069/payment_type
```